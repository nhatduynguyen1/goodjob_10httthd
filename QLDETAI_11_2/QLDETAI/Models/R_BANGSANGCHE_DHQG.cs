using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class R_BANGSANGCHE_DHQG
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string TENBANG { get; set; }
        public string SANPHAM { get; set; }
        public Nullable<System.DateTime> NAMCAP { get; set; }
        public string DONGTACGIA { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
