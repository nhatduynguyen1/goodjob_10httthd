using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class C_LYLICHKHOAHOC
    {
        public C_LYLICHKHOAHOC()
        {
            this.C_LLKH_GIAITHUONG = new List<C_LLKH_GIAITHUONG>();
            this.C_LLKH_NGOAINGU = new List<C_LLKH_NGOAINGU>();
            this.C_LLKH_QTRCONGTAC = new List<C_LLKH_QTRCONGTAC>();
        }

        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string HOTEN { get; set; }
        public Nullable<System.DateTime> NGAYSINH { get; set; }
        public Nullable<bool> GIOITINH { get; set; }
        public string HOCVI { get; set; }
        public Nullable<System.DateTime> NAMDAT { get; set; }
        public string HOCHAM { get; set; }
        public Nullable<System.DateTime> NAMPHONG { get; set; }
        public string DIACHI { get; set; }
        public string DIENTHOAI { get; set; }
        public string EMAIL { get; set; }
        public string LINHVUC { get; set; }
        public string CHUYENNGANH { get; set; }
        public string CHUYENMON { get; set; }
        public string BACDAOTAO { get; set; }
        public Nullable<System.DateTime> THOIGIANDAOTAO { get; set; }
        public string NOIDAOTAO { get; set; }
        public string CHUYENNGANHDAOTAO { get; set; }
        public string COQUAN_DANGCONGTAC { get; set; }
        public string DIACHICOQUAN { get; set; }
        public string DIENTHOAICOQUAN { get; set; }
        public string EMAILCOQUAN { get; set; }
        public virtual ICollection<C_LLKH_GIAITHUONG> C_LLKH_GIAITHUONG { get; set; }
        public virtual ICollection<C_LLKH_NGOAINGU> C_LLKH_NGOAINGU { get; set; }
        public virtual ICollection<C_LLKH_QTRCONGTAC> C_LLKH_QTRCONGTAC { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
