using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class C_LLKH_GIAITHUONG
    {
        public int ID { get; set; }
        public string TENGIAITHUONG { get; set; }
        public string NOIDUNG { get; set; }
        public string NOICAP { get; set; }
        public Nullable<System.DateTime> NAMCAP { get; set; }
        public Nullable<int> IDLLKH { get; set; }
        public virtual C_LYLICHKHOAHOC C_LYLICHKHOAHOC { get; set; }
    }
}
