using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class R_LLKH_SOKHCN
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string TENDETAI { get; set; }
        public Nullable<System.DateTime> THOIGIANBATDAU { get; set; }
        public Nullable<System.DateTime> THOIGIANKETTHUC { get; set; }
        public string CHUONGTRINH { get; set; }
        public Nullable<int> TINHTRANG { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
