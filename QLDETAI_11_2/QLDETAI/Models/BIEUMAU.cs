using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class BIEUMAU
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string TENBIEUMAU { get; set; }
        public string LINKDOWN { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
