using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class CAPDETAI
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string TENCAP { get; set; }
        public string COQUANQUANLI { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
