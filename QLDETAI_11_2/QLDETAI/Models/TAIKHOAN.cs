using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class TAIKHOAN
    {
        public TAIKHOAN()
        {
            this.DETAIs = new List<DETAI>();
        }

        public string TENTAIKHOAN { get; set; }
        public string MATKHAU { get; set; }
        public string EMAIL { get; set; }
        public virtual ICollection<DETAI> DETAIs { get; set; }
    }
}
