using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class BUOCDETAI
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public Nullable<bool> LILICHKHOAHOC { get; set; }
        public Nullable<bool> THUYETMINHDETAI { get; set; }
        public Nullable<bool> DOWLOAD { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
