﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLDETAI.Models
{
    public class HienThiDeTai
    {
        public string Ten { get; set; }
        public int ThoiGianBatDau { get; set; }
        public int ThoiGianKetThuc { get; set; }
        public string CapQuanLy { get; set; }
        public string MaSo { get; set; }
        public string ChuNhiem { get; set; }
        public string CoQuan { get; set; }

    }
}