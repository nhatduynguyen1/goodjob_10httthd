﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace QLDETAI.Models
{
    public class ValidationModels
    {

        /* 1. Chủ nhiệm đề tài */
        /* ------------------- */

        [Required]
        [Display(Name = "Tên chủ nhiệm")]
        [StringLength(30, ErrorMessage = "Tên chủ nhiệm có ít nhất 3 kí tự và nhiều nhất 30 kí tự.", MinimumLength = 3)]
        public string ten_chunhiem { get; set; }

        [Required]
        [Display(Name = "Ngày tháng năm sinh")]
        [RegularExpression(@"1\d|2\d|30|1|2|3|4|5|6|7|8|9", ErrorMessage = "Ngày không hợp lệ")]
        public string namsinh_chunhiem { get; set; }

        [Required]
        [Display(Name = "Giới tính")]
        public string gioitinh_chunhiem { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Tên học vị có ít nhất 5 kí tự.", MinimumLength = 5)]
        [Display(Name = "Học vị")]
        public string hocvi_chunhiem { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Tên chuyên nghành có ít nhất 5 kí tự và nhiều nhất 30 kí tự.", MinimumLength = 5)]
        [Display(Name = "Chuyên nghành")]
        public string chuyennghanh_chunhiem { get; set; }

        [Required]
        [Display(Name = "Năm đạt học vị")]
        [RegularExpression(@"1\d|2\d|30|1|2|3|4|5|6|7|8|9", ErrorMessage = "Ngày không hợp lệ")]
        public string namhocvi_chunhiem { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Tên chức danh khoa học có ít nhất 6 kí tự.", MinimumLength = 3)]
        [Display(Name = "Chức danh khoa học")]
        public string chucdanhkhoahoc_chunhiem { get; set; }

        [Required]
        [Display(Name = "Năm phong chức danh")]
        [RegularExpression(@"1\d|2\d|30|1|2|3|4|5|6|7|8|9", ErrorMessage = "Ngày không hợp lệ")]
        public string namphongchucdanh_chunhiem { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Tên cơ quan công tác có ít nhất 6 kí tự.", MinimumLength = 6)]
        [Display(Name = "Tên cơ quan công tác")]
        public string ten_coquan_chunhiem { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "Địa chỉ cơ quan có ít nhất 10 kí tự.", MinimumLength = 10)]
        [Display(Name = "Địa chỉ cơ quan")]
        public string diachi_coquan_chunhiem { get; set; }

        [Required]
        [Display(Name = "Điện thoại cơ quan chủ nhiệm")]
        [RegularExpression(@"\d{6,11}", ErrorMessage = "Điện thoại cơ quan có từ 6 đến 11 số.")]
        public string dienthoai_coquan_chunhiem { get; set; }

        [Required]
        [Display(Name = "Fax cơ quan chủ nhiệm")]
        [RegularExpression(@"\d{6,11}", ErrorMessage = "Fax cơ quan có từ 6 đến 11 số.")]
        public string fax_coquan_chunhiem { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "Địa chỉ nhà riêng có ít nhất 10 kí tự.", MinimumLength = 10)]
        [Display(Name = "Địa chỉ nhà riêng")]
        public string diachirieng_coquan_chunhiem { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [RegularExpression(@"([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})", ErrorMessage = "Email không hợp lệ.")]
        public string email_coquan_chunhiem { get; set; }

        /* 2. Cơ quan chủ trì và quản lý đề tài */
        /* ------------------------------------ */

        [Required]
        [StringLength(40, ErrorMessage = "Tên cơ quan chủ trì có ít nhất 10 kí tự.", MinimumLength = 10)]
        [Display(Name = "Tên cơ quan chủ trì")]
        public string ten_coquanchutri { get; set; }

        [Required]
        [Display(Name = "Điện thoại cơ quan chủ nhiệm")]
        [RegularExpression(@"\d{6,11}", ErrorMessage = "Điện thoại cơ quan có từ 6 đến 11 số.")]
        public string dienthoai_coquanchutri { get; set; }

        [Required]
        [Display(Name = "Fax cơ quan chủ trì")]
        [RegularExpression(@"\d{6,11}", ErrorMessage = "Fax cơ quan chủ trì có từ 6 đến 11 số.")]
        public string fax_coquanchutri { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email cơ quan chủ trì")]
        [RegularExpression(@"([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})", ErrorMessage = "Email không hợp lệ.")]
        public string email_coquanchutri { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Website cơ quan chủ trì có ít nhất 10 kí tự.", MinimumLength = 10)]
        [Display(Name = "Website")]
        public string website_coquanchutri { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Địa chỉ cơ quan chủ trì có ít nhất 10 kí tự.", MinimumLength = 10)]
        [Display(Name = "Địa chỉ cơ quan chủ trì")]
        public string diachi_coquanchutri { get; set; }

        [Required]
        [Display(Name = "Số tài khoản")]
        [RegularExpression(@"\d{4,12}", ErrorMessage = "Số tài khoản cơ quan có từ 4 đến 12 số.")]
        public string sotaikhoan_coquanchutri { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Ngân hàng có ít nhất 5 kí tự.", MinimumLength = 5)]
        [Display(Name = "Ngân hàng")]
        public string nganhang_coquanchutri { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "Nội dung không được rỗng.", MinimumLength = 1)]
        [Display(Name = "Mục tiêu đề tài")]
        public string muctieu_detai { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "Nội dung không được rỗng.", MinimumLength = 1)]
        [Display(Name = "Tính cấp thiết của đề tài")]
        public string tinhcanthiet_detai { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "Nội dung không được rỗng.", MinimumLength = 1)]
        [Display(Name = "Ý nghĩa và tính mới về khoa học và thực tiễn")]
        public string ynghia_detai { get; set; }

    }
}