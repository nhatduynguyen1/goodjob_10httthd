using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using QLDETAI.Models.Mapping;

namespace QLDETAI.Models
{
    public partial class DBHTTTHDContext : DbContext
    {
        static DBHTTTHDContext()
        {
            Database.SetInitializer<DBHTTTHDContext>(null);
        }

        public DBHTTTHDContext()
            : base("Name=DBHTTTHDContext")
        {
        }

        public DbSet<BIEUMAU> BIEUMAUs { get; set; }
        public DbSet<BUOCDETAI> BUOCDETAIs { get; set; }
        public DbSet<C_LLKH_GIAITHUONG> C_LLKH_GIAITHUONG { get; set; }
        public DbSet<C_LLKH_NGOAINGU> C_LLKH_NGOAINGU { get; set; }
        public DbSet<C_LLKH_QTRCONGTAC> C_LLKH_QTRCONGTAC { get; set; }
        public DbSet<C_LYLICHKHOAHOC> C_LYLICHKHOAHOC { get; set; }
        public DbSet<C_THUYETMINHDETAI> C_THUYETMINHDETAI { get; set; }
        public DbSet<CAPDETAI> CAPDETAIs { get; set; }
        public DbSet<DETAI> DETAIs { get; set; }
        public DbSet<KINHPHI_DETAI> KINHPHI_DETAI { get; set; }
        public DbSet<MOTA_THUYETMINHDETAI> MOTA_THUYETMINHDETAI { get; set; }
        public DbSet<QUANLYDETAI> QUANLYDETAIs { get; set; }
        public DbSet<R_BAIBAO_DHQG> R_BAIBAO_DHQG { get; set; }
        public DbSet<R_BANGSANGCHE_DHQG> R_BANGSANGCHE_DHQG { get; set; }
        public DbSet<R_DETAITUNGTHAMGIA_DHQG> R_DETAITUNGTHAMGIA_DHQG { get; set; }
        public DbSet<R_HUONGDAN_DHQG> R_HUONGDAN_DHQG { get; set; }
        public DbSet<R_LLKH_SOKHCN> R_LLKH_SOKHCN { get; set; }
        public DbSet<R_SACH_DHQG> R_SACH_DHQG { get; set; }
        public DbSet<R_TMDT_DHQG> R_TMDT_DHQG { get; set; }
        public DbSet<TAIKHOAN> TAIKHOANs { get; set; }
        public DbSet<THANHVIEN_THUYETMINHDETAI> THANHVIEN_THUYETMINHDETAI { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BIEUMAUMap());
            modelBuilder.Configurations.Add(new BUOCDETAIMap());
            modelBuilder.Configurations.Add(new C_LLKH_GIAITHUONGMap());
            modelBuilder.Configurations.Add(new C_LLKH_NGOAINGUMap());
            modelBuilder.Configurations.Add(new C_LLKH_QTRCONGTACMap());
            modelBuilder.Configurations.Add(new C_LYLICHKHOAHOCMap());
            modelBuilder.Configurations.Add(new C_THUYETMINHDETAIMap());
            modelBuilder.Configurations.Add(new CAPDETAIMap());
            modelBuilder.Configurations.Add(new DETAIMap());
            modelBuilder.Configurations.Add(new KINHPHI_DETAIMap());
            modelBuilder.Configurations.Add(new MOTA_THUYETMINHDETAIMap());
            modelBuilder.Configurations.Add(new QUANLYDETAIMap());
            modelBuilder.Configurations.Add(new R_BAIBAO_DHQGMap());
            modelBuilder.Configurations.Add(new R_BANGSANGCHE_DHQGMap());
            modelBuilder.Configurations.Add(new R_DETAITUNGTHAMGIA_DHQGMap());
            modelBuilder.Configurations.Add(new R_HUONGDAN_DHQGMap());
            modelBuilder.Configurations.Add(new R_LLKH_SOKHCNMap());
            modelBuilder.Configurations.Add(new R_SACH_DHQGMap());
            modelBuilder.Configurations.Add(new R_TMDT_DHQGMap());
            modelBuilder.Configurations.Add(new TAIKHOANMap());
            modelBuilder.Configurations.Add(new THANHVIEN_THUYETMINHDETAIMap());
        }
    }
}
