using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class R_HUONGDAN_DHQG
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string TENSV_NCS_HVCH { get; set; }
        public string TENLUANAN { get; set; }
        public Nullable<System.DateTime> NAMTOTNGHIEP { get; set; }
        public string BACDAOTAO { get; set; }
        public string SANPHAM { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
