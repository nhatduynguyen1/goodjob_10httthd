using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class R_BAIBAO_DHQG
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string THONGTIN { get; set; }
        public string SANPHAM { get; set; }
        public string SOHIEU_ISSN { get; set; }
        public string GHICHU { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
