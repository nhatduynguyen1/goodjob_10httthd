using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class R_SACH_DHQG
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string TENSACH { get; set; }
        public string SANPHAM { get; set; }
        public string NXB { get; set; }
        public Nullable<System.DateTime> NAMXUATBAN { get; set; }
        public string DONGTACGIA { get; set; }
        public string BUTDANH { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
