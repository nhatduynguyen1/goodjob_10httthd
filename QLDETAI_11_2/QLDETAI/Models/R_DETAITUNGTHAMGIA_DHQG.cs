using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class R_DETAITUNGTHAMGIA_DHQG
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string TENDETAI { get; set; }
        public string MASO_CAPQUANLY { get; set; }
        public Nullable<System.DateTime> THOIGIANTHUCHIEN { get; set; }
        public Nullable<double> KINHPHI { get; set; }
        public Nullable<bool> CHUNHIEM { get; set; }
        public Nullable<System.DateTime> NGAYNGHIEMTHU { get; set; }
        public string KETQUA { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
