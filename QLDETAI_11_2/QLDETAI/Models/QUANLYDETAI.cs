using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class QUANLYDETAI
    {
        public int IDDETAI { get; set; }
        public string TENDETAI { get; set; }
        public string CAPDETAI { get; set; }
        public bool LILICHKHOAHOC { get; set; }
        public bool THUYETMINHDETAI { get; set; }
        public bool DOWNLOAD { get; set; }
    }
}
