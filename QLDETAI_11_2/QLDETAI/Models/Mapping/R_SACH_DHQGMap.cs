using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class R_SACH_DHQGMap : EntityTypeConfiguration<R_SACH_DHQG>
    {
        public R_SACH_DHQGMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENSACH)
                .HasMaxLength(100);

            this.Property(t => t.SANPHAM)
                .HasMaxLength(15);

            this.Property(t => t.NXB)
                .HasMaxLength(50);

            this.Property(t => t.DONGTACGIA)
                .HasMaxLength(50);

            this.Property(t => t.BUTDANH)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("R_SACH_DHQG");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.TENSACH).HasColumnName("TENSACH");
            this.Property(t => t.SANPHAM).HasColumnName("SANPHAM");
            this.Property(t => t.NXB).HasColumnName("NXB");
            this.Property(t => t.NAMXUATBAN).HasColumnName("NAMXUATBAN");
            this.Property(t => t.DONGTACGIA).HasColumnName("DONGTACGIA");
            this.Property(t => t.BUTDANH).HasColumnName("BUTDANH");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.R_SACH_DHQG)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
