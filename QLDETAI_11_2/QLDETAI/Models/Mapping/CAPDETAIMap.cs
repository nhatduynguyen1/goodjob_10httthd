using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class CAPDETAIMap : EntityTypeConfiguration<CAPDETAI>
    {
        public CAPDETAIMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENCAP)
                .HasMaxLength(50);

            this.Property(t => t.COQUANQUANLI)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CAPDETAI");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.TENCAP).HasColumnName("TENCAP");
            this.Property(t => t.COQUANQUANLI).HasColumnName("COQUANQUANLI");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.CAPDETAIs)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
