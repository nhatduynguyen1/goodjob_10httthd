using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class R_BANGSANGCHE_DHQGMap : EntityTypeConfiguration<R_BANGSANGCHE_DHQG>
    {
        public R_BANGSANGCHE_DHQGMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENBANG)
                .HasMaxLength(100);

            this.Property(t => t.SANPHAM)
                .HasMaxLength(15);

            this.Property(t => t.DONGTACGIA)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("R_BANGSANGCHE_DHQG");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.TENBANG).HasColumnName("TENBANG");
            this.Property(t => t.SANPHAM).HasColumnName("SANPHAM");
            this.Property(t => t.NAMCAP).HasColumnName("NAMCAP");
            this.Property(t => t.DONGTACGIA).HasColumnName("DONGTACGIA");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.R_BANGSANGCHE_DHQG)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
