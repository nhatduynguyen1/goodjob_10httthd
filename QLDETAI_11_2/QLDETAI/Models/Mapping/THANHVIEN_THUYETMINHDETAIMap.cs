using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class THANHVIEN_THUYETMINHDETAIMap : EntityTypeConfiguration<THANHVIEN_THUYETMINHDETAI>
    {
        public THANHVIEN_THUYETMINHDETAIMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.THANHVIEN1)
                .HasMaxLength(50);

            this.Property(t => t.THANHVIEN2)
                .HasMaxLength(50);

            this.Property(t => t.THANHVIEN3)
                .HasMaxLength(50);

            this.Property(t => t.THANHVIEN4)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("THANHVIEN_THUYETMINHDETAI");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDTHUYETMINH).HasColumnName("IDTHUYETMINH");
            this.Property(t => t.THANHVIEN1).HasColumnName("THANHVIEN1");
            this.Property(t => t.THANHVIEN2).HasColumnName("THANHVIEN2");
            this.Property(t => t.THANHVIEN3).HasColumnName("THANHVIEN3");
            this.Property(t => t.THANHVIEN4).HasColumnName("THANHVIEN4");

            // Relationships
            this.HasOptional(t => t.C_THUYETMINHDETAI)
                .WithMany(t => t.THANHVIEN_THUYETMINHDETAI)
                .HasForeignKey(d => d.IDTHUYETMINH);

        }
    }
}
