using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class C_LLKH_QTRCONGTACMap : EntityTypeConfiguration<C_LLKH_QTRCONGTAC>
    {
        public C_LLKH_QTRCONGTACMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.NOICONGTAC)
                .HasMaxLength(100);

            this.Property(t => t.CHUCVU)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("C_LLKH_QTRCONGTAC");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.BATDAUTOINAY).HasColumnName("BATDAUTOINAY");
            this.Property(t => t.TGBATDAU).HasColumnName("TGBATDAU");
            this.Property(t => t.TGKETTHUC).HasColumnName("TGKETTHUC");
            this.Property(t => t.NOICONGTAC).HasColumnName("NOICONGTAC");
            this.Property(t => t.CHUCVU).HasColumnName("CHUCVU");
            this.Property(t => t.IDLLKH).HasColumnName("IDLLKH");

            // Relationships
            this.HasOptional(t => t.C_LYLICHKHOAHOC)
                .WithMany(t => t.C_LLKH_QTRCONGTAC)
                .HasForeignKey(d => d.IDLLKH);

        }
    }
}
