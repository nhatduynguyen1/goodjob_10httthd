using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class R_HUONGDAN_DHQGMap : EntityTypeConfiguration<R_HUONGDAN_DHQG>
    {
        public R_HUONGDAN_DHQGMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENSV_NCS_HVCH)
                .HasMaxLength(50);

            this.Property(t => t.TENLUANAN)
                .HasMaxLength(200);

            this.Property(t => t.BACDAOTAO)
                .HasMaxLength(50);

            this.Property(t => t.SANPHAM)
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("R_HUONGDAN_DHQG");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.TENSV_NCS_HVCH).HasColumnName("TENSV_NCS_HVCH");
            this.Property(t => t.TENLUANAN).HasColumnName("TENLUANAN");
            this.Property(t => t.NAMTOTNGHIEP).HasColumnName("NAMTOTNGHIEP");
            this.Property(t => t.BACDAOTAO).HasColumnName("BACDAOTAO");
            this.Property(t => t.SANPHAM).HasColumnName("SANPHAM");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.R_HUONGDAN_DHQG)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
