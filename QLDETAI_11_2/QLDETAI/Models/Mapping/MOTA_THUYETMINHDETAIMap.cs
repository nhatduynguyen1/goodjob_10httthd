using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class MOTA_THUYETMINHDETAIMap : EntityTypeConfiguration<MOTA_THUYETMINHDETAI>
    {
        public MOTA_THUYETMINHDETAIMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("MOTA_THUYETMINHDETAI");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDTHUYETMINH).HasColumnName("IDTHUYETMINH");
            this.Property(t => t.TONGQUAN).HasColumnName("TONGQUAN");
            this.Property(t => t.YTUONG).HasColumnName("YTUONG");
            this.Property(t => t.KETQUASOKHOI).HasColumnName("KETQUASOKHOI");
            this.Property(t => t.KEHOACH).HasColumnName("KEHOACH");

            // Relationships
            this.HasOptional(t => t.C_THUYETMINHDETAI)
                .WithMany(t => t.MOTA_THUYETMINHDETAI)
                .HasForeignKey(d => d.IDTHUYETMINH);

        }
    }
}
