using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class C_LLKH_NGOAINGUMap : EntityTypeConfiguration<C_LLKH_NGOAINGU>
    {
        public C_LLKH_NGOAINGUMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENNGOAINGU)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("C_LLKH_NGOAINGU");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.TENNGOAINGU).HasColumnName("TENNGOAINGU");
            this.Property(t => t.IDLLKH).HasColumnName("IDLLKH");

            // Relationships
            this.HasOptional(t => t.C_LYLICHKHOAHOC)
                .WithMany(t => t.C_LLKH_NGOAINGU)
                .HasForeignKey(d => d.IDLLKH);

        }
    }
}
