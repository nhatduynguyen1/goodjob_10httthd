using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class R_BAIBAO_DHQGMap : EntityTypeConfiguration<R_BAIBAO_DHQG>
    {
        public R_BAIBAO_DHQGMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.SANPHAM)
                .HasMaxLength(15);

            this.Property(t => t.SOHIEU_ISSN)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("R_BAIBAO_DHQG");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.THONGTIN).HasColumnName("THONGTIN");
            this.Property(t => t.SANPHAM).HasColumnName("SANPHAM");
            this.Property(t => t.SOHIEU_ISSN).HasColumnName("SOHIEU_ISSN");
            this.Property(t => t.GHICHU).HasColumnName("GHICHU");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.R_BAIBAO_DHQG)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
