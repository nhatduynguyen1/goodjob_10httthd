using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class KINHPHI_DETAIMap : EntityTypeConfiguration<KINHPHI_DETAI>
    {
        public KINHPHI_DETAIMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.NGUON)
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("KINHPHI_DETAI");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.SOTIEN).HasColumnName("SOTIEN");
            this.Property(t => t.NGUON).HasColumnName("NGUON");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.KINHPHI_DETAI)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
