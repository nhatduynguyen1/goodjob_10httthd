using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class TAIKHOANMap : EntityTypeConfiguration<TAIKHOAN>
    {
        public TAIKHOANMap()
        {
            // Primary Key
            this.HasKey(t => t.TENTAIKHOAN);

            // Properties
            this.Property(t => t.TENTAIKHOAN)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MATKHAU)
                .HasMaxLength(20);

            this.Property(t => t.EMAIL)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TAIKHOAN");
            this.Property(t => t.TENTAIKHOAN).HasColumnName("TENTAIKHOAN");
            this.Property(t => t.MATKHAU).HasColumnName("MATKHAU");
            this.Property(t => t.EMAIL).HasColumnName("EMAIL");
        }
    }
}
