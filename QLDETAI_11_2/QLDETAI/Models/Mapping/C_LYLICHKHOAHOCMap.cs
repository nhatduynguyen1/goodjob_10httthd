using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class C_LYLICHKHOAHOCMap : EntityTypeConfiguration<C_LYLICHKHOAHOC>
    {
        public C_LYLICHKHOAHOCMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.HOTEN)
                .HasMaxLength(50);

            this.Property(t => t.HOCVI)
                .HasMaxLength(100);

            this.Property(t => t.HOCHAM)
                .HasMaxLength(100);

            this.Property(t => t.DIENTHOAI)
                .HasMaxLength(12);

            this.Property(t => t.EMAIL)
                .HasMaxLength(50);

            this.Property(t => t.LINHVUC)
                .HasMaxLength(100);

            this.Property(t => t.CHUYENNGANH)
                .HasMaxLength(100);

            this.Property(t => t.CHUYENMON)
                .HasMaxLength(100);

            this.Property(t => t.BACDAOTAO)
                .HasMaxLength(50);

            this.Property(t => t.NOIDAOTAO)
                .HasMaxLength(100);

            this.Property(t => t.CHUYENNGANHDAOTAO)
                .HasMaxLength(100);

            this.Property(t => t.COQUAN_DANGCONGTAC)
                .HasMaxLength(100);

            this.Property(t => t.DIENTHOAICOQUAN)
                .HasMaxLength(12);

            this.Property(t => t.EMAILCOQUAN)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("C_LYLICHKHOAHOC");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.HOTEN).HasColumnName("HOTEN");
            this.Property(t => t.NGAYSINH).HasColumnName("NGAYSINH");
            this.Property(t => t.GIOITINH).HasColumnName("GIOITINH");
            this.Property(t => t.HOCVI).HasColumnName("HOCVI");
            this.Property(t => t.NAMDAT).HasColumnName("NAMDAT");
            this.Property(t => t.HOCHAM).HasColumnName("HOCHAM");
            this.Property(t => t.NAMPHONG).HasColumnName("NAMPHONG");
            this.Property(t => t.DIACHI).HasColumnName("DIACHI");
            this.Property(t => t.DIENTHOAI).HasColumnName("DIENTHOAI");
            this.Property(t => t.EMAIL).HasColumnName("EMAIL");
            this.Property(t => t.LINHVUC).HasColumnName("LINHVUC");
            this.Property(t => t.CHUYENNGANH).HasColumnName("CHUYENNGANH");
            this.Property(t => t.CHUYENMON).HasColumnName("CHUYENMON");
            this.Property(t => t.BACDAOTAO).HasColumnName("BACDAOTAO");
            this.Property(t => t.THOIGIANDAOTAO).HasColumnName("THOIGIANDAOTAO");
            this.Property(t => t.NOIDAOTAO).HasColumnName("NOIDAOTAO");
            this.Property(t => t.CHUYENNGANHDAOTAO).HasColumnName("CHUYENNGANHDAOTAO");
            this.Property(t => t.COQUAN_DANGCONGTAC).HasColumnName("COQUAN_DANGCONGTAC");
            this.Property(t => t.DIACHICOQUAN).HasColumnName("DIACHICOQUAN");
            this.Property(t => t.DIENTHOAICOQUAN).HasColumnName("DIENTHOAICOQUAN");
            this.Property(t => t.EMAILCOQUAN).HasColumnName("EMAILCOQUAN");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.C_LYLICHKHOAHOC)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
