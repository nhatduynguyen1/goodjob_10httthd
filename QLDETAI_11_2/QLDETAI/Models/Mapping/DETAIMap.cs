using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class DETAIMap : EntityTypeConfiguration<DETAI>
    {
        public DETAIMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TAIKHOAN)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DETAI");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.NGAYTAO).HasColumnName("NGAYTAO");
            this.Property(t => t.TAIKHOAN).HasColumnName("TAIKHOAN");

            // Relationships
            this.HasOptional(t => t.TAIKHOAN1)
                .WithMany(t => t.DETAIs)
                .HasForeignKey(d => d.TAIKHOAN);

        }
    }
}
