using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class R_DETAITUNGTHAMGIA_DHQGMap : EntityTypeConfiguration<R_DETAITUNGTHAMGIA_DHQG>
    {
        public R_DETAITUNGTHAMGIA_DHQGMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENDETAI)
                .HasMaxLength(200);

            this.Property(t => t.MASO_CAPQUANLY)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("R_DETAITUNGTHAMGIA_DHQG");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.TENDETAI).HasColumnName("TENDETAI");
            this.Property(t => t.MASO_CAPQUANLY).HasColumnName("MASO_CAPQUANLY");
            this.Property(t => t.THOIGIANTHUCHIEN).HasColumnName("THOIGIANTHUCHIEN");
            this.Property(t => t.KINHPHI).HasColumnName("KINHPHI");
            this.Property(t => t.CHUNHIEM).HasColumnName("CHUNHIEM");
            this.Property(t => t.NGAYNGHIEMTHU).HasColumnName("NGAYNGHIEMTHU");
            this.Property(t => t.KETQUA).HasColumnName("KETQUA");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.R_DETAITUNGTHAMGIA_DHQG)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
