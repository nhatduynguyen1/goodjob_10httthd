using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class QUANLYDETAIMap : EntityTypeConfiguration<QUANLYDETAI>
    {
        public QUANLYDETAIMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IDDETAI, t.TENDETAI, t.CAPDETAI, t.LILICHKHOAHOC, t.THUYETMINHDETAI, t.DOWNLOAD });

            // Properties
            this.Property(t => t.IDDETAI)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TENDETAI)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.CAPDETAI)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("QUANLYDETAI");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.TENDETAI).HasColumnName("TENDETAI");
            this.Property(t => t.CAPDETAI).HasColumnName("CAPDETAI");
            this.Property(t => t.LILICHKHOAHOC).HasColumnName("LILICHKHOAHOC");
            this.Property(t => t.THUYETMINHDETAI).HasColumnName("THUYETMINHDETAI");
            this.Property(t => t.DOWNLOAD).HasColumnName("DOWNLOAD");
        }
    }
}
