using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class R_TMDT_DHQGMap : EntityTypeConfiguration<R_TMDT_DHQG>
    {
        public R_TMDT_DHQGMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("R_TMDT_DHQG");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.UD_KQ_NGHIENCUU).HasColumnName("UD_KQ_NGHIENCUU");
            this.Property(t => t.PPCHUYENGIAOKETQUA).HasColumnName("PPCHUYENGIAOKETQUA");
            this.Property(t => t.KHOAN_KINHPHI).HasColumnName("KHOAN_KINHPHI");
            this.Property(t => t.KHOAN_KHOANCHI).HasColumnName("KHOAN_KHOANCHI");
            this.Property(t => t.KHOAN_PHANTRAM).HasColumnName("KHOAN_PHANTRAM");
            this.Property(t => t.TONGCONG).HasColumnName("TONGCONG");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.R_TMDT_DHQG)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
