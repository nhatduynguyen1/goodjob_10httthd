using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class C_THUYETMINHDETAIMap : EntityTypeConfiguration<C_THUYETMINHDETAI>
    {
        public C_THUYETMINHDETAIMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENDETAITIENGANH)
                .HasMaxLength(200);

            this.Property(t => t.TENDETAITIENGVIET)
                .HasMaxLength(200);

            this.Property(t => t.NGANH_NHOMNGANH)
                .HasMaxLength(60);

            this.Property(t => t.LOAINGHIENCUU)
                .HasMaxLength(30);

            this.Property(t => t.HOCHAM_HOCVI_TENCHUNHIEM)
                .HasMaxLength(300);

            this.Property(t => t.CMND)
                .HasMaxLength(10);

            this.Property(t => t.NOICAP)
                .HasMaxLength(50);

            this.Property(t => t.SOTAIKHOAN)
                .HasMaxLength(20);

            this.Property(t => t.TENNGANHANG)
                .HasMaxLength(50);

            this.Property(t => t.DIENTHOAI)
                .HasMaxLength(12);

            this.Property(t => t.EMAIL)
                .HasMaxLength(50);

            this.Property(t => t.COQUANCHUTRI)
                .HasMaxLength(100);

            this.Property(t => t.TENTHUTRUONG)
                .HasMaxLength(50);

            this.Property(t => t.COQUANPHOIHOP)
                .HasMaxLength(100);

            this.Property(t => t.TENTHUTRUONGCOQUANPHOIHOP)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("C_THUYETMINHDETAI");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.TENDETAITIENGANH).HasColumnName("TENDETAITIENGANH");
            this.Property(t => t.TENDETAITIENGVIET).HasColumnName("TENDETAITIENGVIET");
            this.Property(t => t.NGANH_NHOMNGANH).HasColumnName("NGANH_NHOMNGANH");
            this.Property(t => t.LOAINGHIENCUU).HasColumnName("LOAINGHIENCUU");
            this.Property(t => t.THOIGIANTHUCHIEN).HasColumnName("THOIGIANTHUCHIEN");
            this.Property(t => t.TONGKINHPHI).HasColumnName("TONGKINHPHI");
            this.Property(t => t.HOCHAM_HOCVI_TENCHUNHIEM).HasColumnName("HOCHAM_HOCVI_TENCHUNHIEM");
            this.Property(t => t.NGAYSINH).HasColumnName("NGAYSINH");
            this.Property(t => t.GIOITINH).HasColumnName("GIOITINH");
            this.Property(t => t.CMND).HasColumnName("CMND");
            this.Property(t => t.NGAYCAP).HasColumnName("NGAYCAP");
            this.Property(t => t.NOICAP).HasColumnName("NOICAP");
            this.Property(t => t.SOTAIKHOAN).HasColumnName("SOTAIKHOAN");
            this.Property(t => t.TENNGANHANG).HasColumnName("TENNGANHANG");
            this.Property(t => t.DIACHICOQUAN).HasColumnName("DIACHICOQUAN");
            this.Property(t => t.DIENTHOAI).HasColumnName("DIENTHOAI");
            this.Property(t => t.EMAIL).HasColumnName("EMAIL");
            this.Property(t => t.TOMTATHOATDONG).HasColumnName("TOMTATHOATDONG");
            this.Property(t => t.COQUANCHUTRI).HasColumnName("COQUANCHUTRI");
            this.Property(t => t.TENTHUTRUONG).HasColumnName("TENTHUTRUONG");
            this.Property(t => t.COQUANPHOIHOP).HasColumnName("COQUANPHOIHOP");
            this.Property(t => t.TENTHUTRUONGCOQUANPHOIHOP).HasColumnName("TENTHUTRUONGCOQUANPHOIHOP");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.C_THUYETMINHDETAI)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
