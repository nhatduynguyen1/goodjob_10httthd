using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class C_LLKH_GIAITHUONGMap : EntityTypeConfiguration<C_LLKH_GIAITHUONG>
    {
        public C_LLKH_GIAITHUONGMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENGIAITHUONG)
                .HasMaxLength(200);

            this.Property(t => t.NOICAP)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("C_LLKH_GIAITHUONG");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.TENGIAITHUONG).HasColumnName("TENGIAITHUONG");
            this.Property(t => t.NOIDUNG).HasColumnName("NOIDUNG");
            this.Property(t => t.NOICAP).HasColumnName("NOICAP");
            this.Property(t => t.NAMCAP).HasColumnName("NAMCAP");
            this.Property(t => t.IDLLKH).HasColumnName("IDLLKH");

            // Relationships
            this.HasOptional(t => t.C_LYLICHKHOAHOC)
                .WithMany(t => t.C_LLKH_GIAITHUONG)
                .HasForeignKey(d => d.IDLLKH);

        }
    }
}
