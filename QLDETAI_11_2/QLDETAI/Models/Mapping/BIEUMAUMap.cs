using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class BIEUMAUMap : EntityTypeConfiguration<BIEUMAU>
    {
        public BIEUMAUMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENBIEUMAU)
                .HasMaxLength(50);

            this.Property(t => t.LINKDOWN)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("BIEUMAU");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.TENBIEUMAU).HasColumnName("TENBIEUMAU");
            this.Property(t => t.LINKDOWN).HasColumnName("LINKDOWN");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.BIEUMAUs)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
