using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class R_LLKH_SOKHCNMap : EntityTypeConfiguration<R_LLKH_SOKHCN>
    {
        public R_LLKH_SOKHCNMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TENDETAI)
                .HasMaxLength(200);

            this.Property(t => t.CHUONGTRINH)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("R_LLKH_SOKHCN");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.TENDETAI).HasColumnName("TENDETAI");
            this.Property(t => t.THOIGIANBATDAU).HasColumnName("THOIGIANBATDAU");
            this.Property(t => t.THOIGIANKETTHUC).HasColumnName("THOIGIANKETTHUC");
            this.Property(t => t.CHUONGTRINH).HasColumnName("CHUONGTRINH");
            this.Property(t => t.TINHTRANG).HasColumnName("TINHTRANG");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.R_LLKH_SOKHCN)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
