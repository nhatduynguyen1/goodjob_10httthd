using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace QLDETAI.Models.Mapping
{
    public class BUOCDETAIMap : EntityTypeConfiguration<BUOCDETAI>
    {
        public BUOCDETAIMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("BUOCDETAI");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IDDETAI).HasColumnName("IDDETAI");
            this.Property(t => t.LILICHKHOAHOC).HasColumnName("LILICHKHOAHOC");
            this.Property(t => t.THUYETMINHDETAI).HasColumnName("THUYETMINHDETAI");
            this.Property(t => t.DOWLOAD).HasColumnName("DOWLOAD");

            // Relationships
            this.HasOptional(t => t.DETAI)
                .WithMany(t => t.BUOCDETAIs)
                .HasForeignKey(d => d.IDDETAI);

        }
    }
}
