using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class KINHPHI_DETAI
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public Nullable<double> SOTIEN { get; set; }
        public string NGUON { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
