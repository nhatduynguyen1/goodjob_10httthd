using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class C_THUYETMINHDETAI
    {
        public C_THUYETMINHDETAI()
        {
            this.MOTA_THUYETMINHDETAI = new List<MOTA_THUYETMINHDETAI>();
            this.THANHVIEN_THUYETMINHDETAI = new List<THANHVIEN_THUYETMINHDETAI>();
        }

        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string TENDETAITIENGANH { get; set; }
        public string TENDETAITIENGVIET { get; set; }
        public string NGANH_NHOMNGANH { get; set; }
        public string LOAINGHIENCUU { get; set; }
        public Nullable<System.DateTime> THOIGIANTHUCHIEN { get; set; }
        public Nullable<double> TONGKINHPHI { get; set; }
        public string HOCHAM_HOCVI_TENCHUNHIEM { get; set; }
        public Nullable<System.DateTime> NGAYSINH { get; set; }
        public Nullable<bool> GIOITINH { get; set; }
        public string CMND { get; set; }
        public Nullable<System.DateTime> NGAYCAP { get; set; }
        public string NOICAP { get; set; }
        public string SOTAIKHOAN { get; set; }
        public string TENNGANHANG { get; set; }
        public string DIACHICOQUAN { get; set; }
        public string DIENTHOAI { get; set; }
        public string EMAIL { get; set; }
        public string TOMTATHOATDONG { get; set; }
        public string COQUANCHUTRI { get; set; }
        public string TENTHUTRUONG { get; set; }
        public string COQUANPHOIHOP { get; set; }
        public string TENTHUTRUONGCOQUANPHOIHOP { get; set; }
        public virtual DETAI DETAI { get; set; }
        public virtual ICollection<MOTA_THUYETMINHDETAI> MOTA_THUYETMINHDETAI { get; set; }
        public virtual ICollection<THANHVIEN_THUYETMINHDETAI> THANHVIEN_THUYETMINHDETAI { get; set; }
    }
}
