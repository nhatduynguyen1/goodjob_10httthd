using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class C_LLKH_NGOAINGU
    {
        public int ID { get; set; }
        public string TENNGOAINGU { get; set; }
        public Nullable<int> IDLLKH { get; set; }
        public virtual C_LYLICHKHOAHOC C_LYLICHKHOAHOC { get; set; }
    }
}
