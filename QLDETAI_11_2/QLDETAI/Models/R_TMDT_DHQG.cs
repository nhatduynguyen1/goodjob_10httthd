using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class R_TMDT_DHQG
    {
        public int ID { get; set; }
        public Nullable<int> IDDETAI { get; set; }
        public string UD_KQ_NGHIENCUU { get; set; }
        public string PPCHUYENGIAOKETQUA { get; set; }
        public Nullable<double> KHOAN_KINHPHI { get; set; }
        public Nullable<double> KHOAN_KHOANCHI { get; set; }
        public Nullable<int> KHOAN_PHANTRAM { get; set; }
        public Nullable<double> TONGCONG { get; set; }
        public virtual DETAI DETAI { get; set; }
    }
}
