using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class MOTA_THUYETMINHDETAI
    {
        public int ID { get; set; }
        public Nullable<int> IDTHUYETMINH { get; set; }
        public string TONGQUAN { get; set; }
        public string YTUONG { get; set; }
        public string KETQUASOKHOI { get; set; }
        public string KEHOACH { get; set; }
        public virtual C_THUYETMINHDETAI C_THUYETMINHDETAI { get; set; }
    }
}
