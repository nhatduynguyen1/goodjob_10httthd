using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class C_LLKH_QTRCONGTAC
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> BATDAUTOINAY { get; set; }
        public Nullable<System.DateTime> TGBATDAU { get; set; }
        public Nullable<System.DateTime> TGKETTHUC { get; set; }
        public string NOICONGTAC { get; set; }
        public string CHUCVU { get; set; }
        public Nullable<int> IDLLKH { get; set; }
        public virtual C_LYLICHKHOAHOC C_LYLICHKHOAHOC { get; set; }
    }
}
