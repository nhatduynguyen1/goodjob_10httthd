using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class DETAI
    {
        public DETAI()
        {
            this.BIEUMAUs = new List<BIEUMAU>();
            this.BUOCDETAIs = new List<BUOCDETAI>();
            this.C_LYLICHKHOAHOC = new List<C_LYLICHKHOAHOC>();
            this.C_THUYETMINHDETAI = new List<C_THUYETMINHDETAI>();
            this.CAPDETAIs = new List<CAPDETAI>();
            this.R_DETAITUNGTHAMGIA_DHQG = new List<R_DETAITUNGTHAMGIA_DHQG>();
            this.R_HUONGDAN_DHQG = new List<R_HUONGDAN_DHQG>();
            this.R_SACH_DHQG = new List<R_SACH_DHQG>();
            this.R_BAIBAO_DHQG = new List<R_BAIBAO_DHQG>();
            this.R_BANGSANGCHE_DHQG = new List<R_BANGSANGCHE_DHQG>();
            this.R_LLKH_SOKHCN = new List<R_LLKH_SOKHCN>();
            this.KINHPHI_DETAI = new List<KINHPHI_DETAI>();
            this.R_TMDT_DHQG = new List<R_TMDT_DHQG>();
        }

        public int ID { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string TAIKHOAN { get; set; }
        public virtual ICollection<BIEUMAU> BIEUMAUs { get; set; }
        public virtual ICollection<BUOCDETAI> BUOCDETAIs { get; set; }
        public virtual ICollection<C_LYLICHKHOAHOC> C_LYLICHKHOAHOC { get; set; }
        public virtual ICollection<C_THUYETMINHDETAI> C_THUYETMINHDETAI { get; set; }
        public virtual ICollection<CAPDETAI> CAPDETAIs { get; set; }
        public virtual ICollection<R_DETAITUNGTHAMGIA_DHQG> R_DETAITUNGTHAMGIA_DHQG { get; set; }
        public virtual ICollection<R_HUONGDAN_DHQG> R_HUONGDAN_DHQG { get; set; }
        public virtual ICollection<R_SACH_DHQG> R_SACH_DHQG { get; set; }
        public virtual ICollection<R_BAIBAO_DHQG> R_BAIBAO_DHQG { get; set; }
        public virtual ICollection<R_BANGSANGCHE_DHQG> R_BANGSANGCHE_DHQG { get; set; }
        public virtual ICollection<R_LLKH_SOKHCN> R_LLKH_SOKHCN { get; set; }
        public virtual ICollection<KINHPHI_DETAI> KINHPHI_DETAI { get; set; }
        public virtual ICollection<R_TMDT_DHQG> R_TMDT_DHQG { get; set; }
        public virtual TAIKHOAN TAIKHOAN1 { get; set; }
    }
}
