using System;
using System.Collections.Generic;

namespace QLDETAI.Models
{
    public partial class THANHVIEN_THUYETMINHDETAI
    {
        public int ID { get; set; }
        public Nullable<int> IDTHUYETMINH { get; set; }
        public string THANHVIEN1 { get; set; }
        public string THANHVIEN2 { get; set; }
        public string THANHVIEN3 { get; set; }
        public string THANHVIEN4 { get; set; }
        public virtual C_THUYETMINHDETAI C_THUYETMINHDETAI { get; set; }
    }
}
