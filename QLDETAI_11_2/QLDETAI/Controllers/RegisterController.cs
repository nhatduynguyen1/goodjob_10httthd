﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.Entity;
using HtmlAgilityPack;
using System.Globalization;
using QLDETAI.Services;
using QLDETAI.Models;

namespace QLDETAI.Controllers
{
    public class RegisterController : Controller
    {
        DBHTTTHDContext db = new DBHTTTHDContext();
        //
        // GET: /Register/

        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult BasicLevel_R()
        {

            if (User.Identity.IsAuthenticated)
            {
                
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        //private void XuLyUpload(string Title)
        //{
        //    if (Title != null)
        //    {
        //        if (Request.Files["file"].ContentLength > 0)
        //        {
        //            string extension = System.IO.Path.GetExtension(Request.Files["file"].FileName);
        //            string path1 = string.Format("{0}/{1}{2}", Server.MapPath("~/File"), Title, extension);
        //            if (System.IO.File.Exists(path1))
        //                System.IO.File.Delete(path1);
        //            DE_TAI detai = new DE_TAI();
        //            detai.LINK_FILE_THUYET_MINH_DE_TAI = path1;

        //            db.DE_TAI.Add(detai);
        //            db.SaveChanges();
        //            Request.Files["file"].SaveAs(path1);
        //            var query =
        //                        from dt in db.DE_TAI
        //                        where dt.LINK_FILE_THUYET_MINH_DE_TAI == path1
        //                        select dt;
        //            if (query != null)
        //            {
        //                CultureInfo myCIintl = new CultureInfo("vi-VN", false);     // Note new

        //                 Để bỏ vào database
        //                foreach (DE_TAI dt in query)
        //                {
        //                    SoKHCN_TMDT tmdt = new SoKHCN_TMDT(@"" + dt.LINK_FILE_THUYET_MINH_DE_TAI + "");
        //                    ViewBag.Success = "Success";
        //                    dt.TEN_DE_TAI = tmdt.tenDeTai;
        //                    ViewBag.TEN_DE_TAI = tmdt.tenDeTai;
        //                    dt.NGAY_BAT_DAU = tmdt.ngayBatDau;
        //                    ViewBag.NGAY_BAT_DAU = tmdt.ngayBatDau;
        //                    dt.NGAY_KET_THUC = tmdt.ngayKetThuc;
        //                    ViewBag.NGAY_KET_THUC = tmdt.ngayKetThuc;
        //                    dt.THOI_GIAN_THUC_HIEN = tmdt.thoiGianThucHien;
        //                    ViewBag.THOI_GIAN_THUC_HIEN = tmdt.thoiGianThucHien;
        //                    dt.KINH_PHI_THUC_HIEN = tmdt.kinhPhiThucHien;
        //                    ViewBag.KINH_PHI_THUC_HIEN = tmdt.kinhPhiThucHien;
        //                    dt.KINH_PHI_TU_NGAN_SACH_ = tmdt.kinhPhiTuNganSach;
        //                    ViewBag.KINH_PHI_TU_NGAN_SACH_ = tmdt.kinhPhiTuNganSach;
        //                    dt.KINH_PHI_TU_NGUON_KHAC = tmdt.kinhPhiTuNguonKhac;
        //                    ViewBag.KINH_PHI_TU_NGUON_KHAC = tmdt.kinhPhiTuNguonKhac;
        //                    dt.CONG_VAN = tmdt.congVan;
        //                    ViewBag.CONG_VAN = tmdt.congVan;
        //                    dt.MASO = tmdt.maDeTai;
        //                    ViewBag.MASO = tmdt.maDeTai;
        //                    db.Entry(dt).State = EntityState.Modified;
        //                }
        //                db.SaveChanges();
        //                 Bỏ vào form

        //            }
        //        }

        //    }
        //    Xử lý link và lưu vào csdl
        //}
        [AllowAnonymous]
        public ActionResult SoKHCNLevel_R(string Title)
        {
            if (User.Identity.IsAuthenticated)
            {
                List<SelectListItem> items = new List<SelectListItem>();
                items.Add(new SelectListItem { Text = "Thường", Value = "0", Selected = true });
                items.Add(new SelectListItem { Text = "Chủ nhiệm", Value = "1" });
                ViewBag.MemberType = items;
                //Code upload file
                //XuLyUpload(Title);
                //Xử lý link và lưu vào csdl

               
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [AllowAnonymous]
        public ActionResult ThuyetMinhDeTai()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ThuyetMinhDeTai(string Title)
        {
            //XuLyUpload(Title);
            return View();
        }

        [AllowAnonymous]
        public ActionResult BieuMau()
        {
            return View();
        }
    }
}
