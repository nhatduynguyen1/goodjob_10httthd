﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLDETAI.Models;
using PagedList;

namespace QLDETAI.Controllers
{
    public class ManageController : Controller
    {
        //
        // GET: /Manage/
        DBHTTTHDContext db = new DBHTTTHDContext();
        public ActionResult Index()
        {
            string user = User.Identity.Name.ToString();
            QUANLYDETAI ql = new QUANLYDETAI();
            try
            {
                IEnumerable<int> maDeTai = db.DETAIs.Where(a => a.TAIKHOAN.ToUpper().Contains(user.ToUpper())).Select(a => a.ID).AsEnumerable<int>();
                foreach (int m in maDeTai)
                {
                    ql.IDDETAI = m;
                    string tenDeTai = (string)db.C_THUYETMINHDETAI.Where(a => a.IDDETAI == m).Select(a => a.TENDETAITIENGVIET).FirstOrDefault();
                    string capDeTai = (string)db.CAPDETAIs.Where(a => a.IDDETAI == m).Select(a => a.TENCAP).FirstOrDefault();
                    bool LLKH = (bool)db.BUOCDETAIs.Where(a => a.IDDETAI == m).Select(a => a.LILICHKHOAHOC).FirstOrDefault();
                    bool TMDT = (bool)db.BUOCDETAIs.Where(a => a.IDDETAI == m).Select(a => a.THUYETMINHDETAI).FirstOrDefault();
                    bool DL = (bool)db.BUOCDETAIs.Where(a => a.IDDETAI == m).Select(a => a.DOWLOAD).FirstOrDefault();
                    ql.TENDETAI = tenDeTai;
                    ql.CAPDETAI = capDeTai;
                    ql.LILICHKHOAHOC = LLKH;
                    ql.THUYETMINHDETAI = TMDT;
                    ql.DOWNLOAD = DL;
                    db.QUANLYDETAIs.Add(ql);
                }
                db.SaveChanges();
            }
            catch { }
            IEnumerable<QUANLYDETAI> ql1 = db.QUANLYDETAIs.ToList();
            return View(ql1);
        }


    }
}
