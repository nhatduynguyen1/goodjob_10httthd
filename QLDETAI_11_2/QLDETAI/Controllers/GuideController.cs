﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDETAI.Controllers
{
    public class GuideController : Controller
    {
        //
        // GET: /Guide/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult BasicLevel()
        {
            return View();
        }
        public ActionResult BasicLevel_2()
        {
            return View();
        }
        public ActionResult BasicLevel_3()
        {
            return View();
        }
        public ActionResult BasicLevel_4()
        {
            return View();
        }
        public ActionResult CapTruong()
        {
            return View();
        }
        public ActionResult CapTruong_1()
        {
            return View();
        }
        public ActionResult CapTruong_2()
        {
            return View();
        }
        public ActionResult CapTruong_3()
        {
            return View();
        }
        public ActionResult DHQG_ABLevel()
        {
            return View();
        }
        public ActionResult DHQG_ABLevel_2()
        {
            return View();
        }
        public ActionResult DHQG_ABLevel_3()
        {
            return View();
        }
        public ActionResult DHQG_ABLevel_4()
        {
            return View();
        }
        public ActionResult DHQG_CLevel()
        {
            return View();
        }
        public ActionResult DHQG_CLevel_2()
        {
            return View();
        }
        public ActionResult DHQG_CLevel_3()
        {
            return View();
        }
        public ActionResult DHQG_CLevel_4()
        {
            return View();
        }
        // GUI cấp cơ sở 
        public ActionResult BasicLevel1_Coso()
        {
            return View();
        }
        public ActionResult BasicLevel2_Coso()
        {
            return View();
        }
        public ActionResult BasicLevel3_Coso()
        {
            return View();
        }
        public ActionResult BasicLevel4_Coso()
        {
            return View();
        }
        public ActionResult BasicLevel5_Coso()
        {
            return View();
        }
    }
}
