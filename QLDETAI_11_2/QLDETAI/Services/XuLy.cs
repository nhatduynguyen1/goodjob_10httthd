﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace QLDETAI.Services
{
    public class XuLy
    {
        SqlConnection connection;
        public XuLy(SqlConnection connection) //Khai báo xử lý mới phải có connect DB
        {
            this.connection = connection;
        }
        public XuLy() { }
        public string LoaiTuDungTruoc(string input, string tuDungTruoc, int viTri) // Loại tuDungTruoc
        {
            // if ( viTri == 0 ) -- Dự trữ
            //string stringtemp = input.ToLower().Replace(tuDungTruoc.ToLower(), "");
            string stringtemp = input.Substring(tuDungTruoc.Length);
            stringtemp = ChuanHoaTu(stringtemp);
            return stringtemp;
        }

        public string ChuanHoaTu(string input) // Xử lý từ, Note : chỉ mới loại khoảng trắng
        {
            input = Regex.Replace(input, @"\s+", " "); // Loại 2 khoảng trắng liên tiếp + khoảng trắng cuối từ Note : thư viên Regex ???
            input = input.Trim(); // Loại khoảng trắng đầu từ
            return input;
        }

        public string XoaTu(string input, string tuCanXoa) // thay thế hàm Replace , do hàm này phân biệt chữ hoa, thường
        {
            string inputtemp = ChuanHoaTu(input); // Xóa khoảng trắng dư
            string tuCanXoatemp = ChuanHoaTu(tuCanXoa); // Xóa khoảng trắng dư
            int viTriTuCanXoa = inputtemp.ToLower().IndexOf(tuCanXoatemp.ToLower()); // Tìm vị trí từ cần xóa
            string tuXoa = inputtemp.Substring(viTriTuCanXoa, tuCanXoatemp.Length);  //Lấy từ ở vị trí trên
            inputtemp = inputtemp.Replace(tuXoa, "");                               // Xóa từ vừa tìm dc
            return inputtemp;
        }
    }
}