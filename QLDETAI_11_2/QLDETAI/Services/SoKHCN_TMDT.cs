﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using System.Text;
using System.Threading.Tasks;
using QLDETAI.Services;

namespace QLDETAI.Services
{
    public class SoKHCN_TMDT
    {
        HtmlDocument doc;
        XuLy xuly; // Xử lý chuỗi

        /* -------------------------Nội dung file doc----------------------*/
        public string tenDeTai;
        public string maDeTai;
        public int loaiDeTai;                           // 0 : chưa chọn, 1: cơ bản , 2: triển khai, -1: lỗi chọn cả 2 ô
        public DateTime ngayBatDau;
        public DateTime ngayKetThuc;
        public int thoiGianThucHien;
        public string capQuanLy;
        public int kinhPhiThucHien;                     // -2 : chưa điền
        public int kinhPhiTuNganSach;
        public int kinhPhiTuNguonKhac;
        public string nguonKinhPhiKhac;
        public int loaiChuongTrinh;                         // 1 : thuộc chương trình , 2 : tự đề xuất , 3 : đặt hàng
        public string chuongTrinh;
        public string congVan;
        public string coQuanChuTriVaQuanLy;

        /*-----------------------------------------------------------------*/

        public SoKHCN_TMDT(string file)                 // Khai báo tài liệu mới - input link đến file
        {
            //string path = "F:\\test_03.doc";
            file = chuyenThanhLinkDung(file);
            Console.WriteLine(file);
            doc = new HtmlDocument();
            doc.Load(file);                          // Note : cần thêm hàm kiểm tra file có tồn tại hay ko 
            xuly = new XuLy();                          // Gọi Class xử lý chuỗi, Note: Xử lý tách từ project XuLyDiaDiem, cần bổ sung

            /*----- Lấy nội dung -----*/
            // Note : Hầu hết các hàm đều chưa xử lý việc nhập sai kiểu ( ký tự lạ, chữ vào ô số )
            tenDeTai = layTenDeTai();
            maDeTai = layMaDeTai();
            loaiDeTai = layLoaiDeTai();
            thoiGianThucHien = layThoiGianThucHien(ref ngayBatDau, ref ngayKetThuc);
            capQuanLy = layCapQuanLy();
            kinhPhiThucHien = layKinhPhiThucHien();
            kinhPhiTuNganSach = layKinhPhiTuNganSach();
            kinhPhiTuNguonKhac = layKinhPhiTuNguonKhac(ref nguonKinhPhiKhac);
            loaiChuongTrinh = layLoaiChuongTrinh(ref chuongTrinh, ref congVan);
            coQuanChuTriVaQuanLy = layCoQuanChuTriVaQuanLy();
        }

        public string chuyenThanhLinkDung(string input)
        {
            string link;
            link = input.Replace(@"/", @"\").Replace(@"\\", @"\").Replace(@"\", @"\\");
            link = @"" + link;
            return link;
        }

        string layTenDeTai()                            // Tên đề tài
        {
            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[2]/td[1]");
            if (boxContent == null)                     // Note : nên thay = try catch để hiện lỗi
                return "Khong doc duoc !!!!!!!!!!!!!!!!";
            string onlyInput = boxContent.InnerText;               // Chỉ lấy nôi dung sau dấu ":"
            onlyInput = xuly.ChuanHoaTu(onlyInput);     // Loại bỏ space dư thừa
            //Console.WriteLine(onlyInput);
            return onlyInput;
        }

        string layMaDeTai()                             // Mã số đề tài
        {
            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[2]/td[2]");
            if (boxContent == null)
                return "Khong doc duoc !!!!!!!!!!!!!!!!";
            string onlyInput = boxContent.InnerText;               // Chỉ lấy nôi dung sau dấu ":"
            onlyInput = xuly.ChuanHoaTu(onlyInput);
            //Console.WriteLine(onlyInput);
            return onlyInput;
        }

        int layLoaiDeTai()                              // Loại đề tài, Note : mở rộng output là List ô check
        {
            int loai = 0;                               // loai = 0 -> chưa chọn loại nào
            HtmlAgilityPack.HtmlNode boxContent1 = doc.DocumentNode.SelectSingleNode("//tr[3]/td[3]/p");            // Lấy giá trị 2 ô loại
            HtmlAgilityPack.HtmlNode boxContent2 = doc.DocumentNode.SelectSingleNode("//tr[4]/td[1]/p");             // Note : Ko hiểu sao lại khác cả tr và td, cần sửa lại để chạy for có tính mở rộng hơn

            if (boxContent1.InnerText.ToLower().Contains("x"))               // Ô 1 có check x
            {
                loai = 1;
            }
            if (boxContent2.InnerText.ToLower().Contains("x"))
            {
                if (loai > 0)
                    return -1;                                              // Phát hiện lỗi check 2 ô
                loai = 2;
            }
            return loai;
        }

        int layThoiGianThucHien(ref DateTime ngayBatDau, ref DateTime ngayKetThuc)                                   // Note new : thay đổi giá trị parameter - ref
        {

            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[6]/td[1]/p");
            string chuoiNgayBatDau = boxContent.InnerText.Substring(boxContent.InnerText.IndexOf("Từ tháng") + 9, boxContent.InnerText.IndexOf("đến") - (boxContent.InnerText.IndexOf("Từ tháng") + 14)).Replace(".", "").Replace(" ", ""); //Note : số 14, 16... : thử và sai :))
            string chuoiNgayKetThuc = boxContent.InnerText.Substring(boxContent.InnerText.IndexOf("đến") + 12, boxContent.InnerText.IndexOf(")") - (boxContent.InnerText.IndexOf("đến") + 16)).Replace(".", "").Replace(" ", "");
            ngayBatDau = DateTime.ParseExact(chuoiNgayBatDau, "M/yyyy", null);                                      // Note : ngoại lệ tháng chỉ có 1 ngày -> M thay MM
            ngayKetThuc = DateTime.ParseExact(chuoiNgayKetThuc, "M/yyyy", null);
            int soThang = ((ngayKetThuc.Year - ngayBatDau.Year) * 12) + ngayKetThuc.Month - ngayBatDau.Month;
            return soThang;
        }

        string layCapQuanLy()                       // Cấp quản lý đề tài
        {
            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[6]/td[2]/p");
            if (boxContent == null)
                return "Khong doc duoc !!!!!!!!!!!!!!!!";
            string onlyInput = boxContent.InnerText;
            onlyInput = xuly.ChuanHoaTu(onlyInput);
            return onlyInput;
        }

        int layKinhPhiThucHien()                       // Kinh phí thực hiện
        {
            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[7]/td[2]/p/strong");
            if (boxContent == null)
                return -1;                              // Không lấy dc
            string onlyInput = boxContent.InnerText.Substring(boxContent.InnerText.IndexOf("phí") + 4, boxContent.InnerText.IndexOf("ngàn") - (boxContent.InnerText.IndexOf("phí") + 4)).Replace(" ", "").Replace(".", "");
            if (onlyInput == "")
                return -2;
            int kinhphi = Convert.ToInt32(onlyInput);   // Note : số lớn vượt kiểu int thì sao ???
            return kinhphi;
        }

        int layKinhPhiTuNganSach()                       // Kinh phí từ ngân sách
        {
            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[9]/td[2]");
            if (boxContent == null)
                return -1;                              // Không lấy dc
            string onlyInput = xuly.ChuanHoaTu(boxContent.InnerText).Replace(".", "").Replace(" ", "");
            int kinhphi = Convert.ToInt32(onlyInput);
            return kinhphi;
        }

        int layKinhPhiTuNguonKhac(ref string nguonKinhPhiKhac)      // Kinh phí từ nguồn ngoài
        {
            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[10]/td[2]/p");
            if (boxContent == null)
                return -1;
            if (!(boxContent.InnerText.Contains("(") && boxContent.InnerText.Contains(")")))
                return -2;                              // Không ghi rõ nguồn ngoài                         // Không lấy dc
            string onlyInput = xuly.ChuanHoaTu(boxContent.InnerText.Substring(0, boxContent.InnerText.IndexOf("("))).Replace(".", "").Replace(" ", "");
            int kinhphi = Convert.ToInt32(onlyInput);
            nguonKinhPhiKhac = boxContent.InnerText.Substring(boxContent.InnerText.IndexOf("(") + 1, boxContent.InnerText.IndexOf(")") - boxContent.InnerText.IndexOf("(") - 1);
            nguonKinhPhiKhac = xuly.ChuanHoaTu(nguonKinhPhiKhac);
            return kinhphi;
        }

        string layChuongTrinh()                         // Lấy chương trình đề tài
        {

            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[11]/td[3]/p");
            if (boxContent == null)
                return "Khong doc duoc !!!!!!!!!!!!!!!!";
            string onlyInput = boxContent.InnerText.Substring(boxContent.InnerText.IndexOf(":") + 1);
            onlyInput = xuly.ChuanHoaTu(onlyInput);
            return onlyInput;
        }

        string layCongVan()
        {
            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[14]/td[2]/p");
            if (boxContent == null)
                return "Khong doc duoc !!!!!!!!!!!!!!!!";
            string onlyInput = boxContent.InnerText.Substring(boxContent.InnerText.IndexOf(":") + 1, boxContent.InnerText.IndexOf(")") - (boxContent.InnerText.IndexOf(":") + 2)).Replace(".", "");
            onlyInput = xuly.ChuanHoaTu(onlyInput).Replace(".", "");
            return onlyInput;
        }

        int layLoaiChuongTrinh(ref string chuongTrinh, ref string congVan)    // Loại đề tài : Chương trình / tự đề xuất / công văn
        {
            int loai = 0;                               // loai = 0 -> chưa chọn loại nào
            chuongTrinh = "";
            congVan = "";
            HtmlAgilityPack.HtmlNode boxContent1 = doc.DocumentNode.SelectSingleNode("//tr[11]/td[2]/p");
            HtmlAgilityPack.HtmlNode boxContent2 = doc.DocumentNode.SelectSingleNode("//tr[13]/td[1]/p");
            HtmlAgilityPack.HtmlNode boxContent3 = doc.DocumentNode.SelectSingleNode("//tr[14]/td[1]/p");

            if (boxContent1.InnerText.ToLower().Contains("x"))               // Ô 1 có check x
            {
                loai = 1;
                chuongTrinh = layChuongTrinh();
            }
            if (boxContent2.InnerText.ToLower().Contains("x"))
            {
                if (loai > 0)
                    return -1;                                              // Phát hiện lỗi check 2 ô
                loai = 2;
            }
            if (boxContent3.InnerText.ToLower().Contains("x"))
            {
                if (loai > 0)
                    return -1;                                              // Phát hiện lỗi check 2 ô
                loai = 3;
                congVan = layCongVan();
            }
            return loai;
        }
        string layCoQuanChuTriVaQuanLy()
        {
            HtmlAgilityPack.HtmlNode boxContent = doc.DocumentNode.SelectSingleNode("//tr[15]/td[2]/p/strong");
            if (boxContent == null)
                return "Khong doc duoc !!!!!!!!!!!!!!!!";
            string onlyInput = boxContent.InnerText.Substring(boxContent.InnerText.IndexOf(":") + 1);
            onlyInput = xuly.ChuanHoaTu(onlyInput);
            return onlyInput;
        }
    }
}